/// <reference types="cypress" />

import { UI_HOST } from "./../support/consts";

describe("Exams - Simple Test", () => {
  beforeEach(() => {
    cy.restoreLocalStorage();
  });

  afterEach(() => {
    cy.saveLocalStorage();
  });

  before(() => {
    cy.getToken();
    cy.createUser();
  });

  it("Login", () => {
    console.log(UI_HOST);
    cy.visit(UI_HOST);
    cy.fixture("user").should(({ user }) => {
      console.log(user);
      cy.get('[placeholder="Usuário"]').type(user.name);
      cy.get('[placeholder="Senha"]').type(user.name);
    });
    cy.wait(1000);
    cy.get(".primary-btn").click();
  });

  it("Confirm Session", () => {
    cy.wait(5000);
    cy.location().then(({ pathname }) => {
      console.log(pathname);
      if (pathname === "/login") {
        cy.get("#root > section > p:nth-child(3)").contains(
          "Deseja iniciar uma nova sessão?"
        );
        cy.get("#root > section > div > button.primary-btn").click();
      }
    });
  });

  it("Choose first application avaiable application", () => {
    cy.wait(3000);
    cy.get('span:contains("Disponível"), span:contains("Iniciada")').click();
  });

  it("Start or continue application", () => {
    cy.wait(3000);
    cy.get('button:contains("continuar"), button:contains("iniciar")').click();
  });

  it("Choose randomly alternative go to the next item", () => {
    const loopArray = Array.from({ length: 1000 }, (v, k) => k + 1);
    cy.wrap(loopArray).each(() => {
      cy.wait(3000);
      const randomAlternativeIndex = Math.floor(Math.random() * 4);
      const randomAlternative = String.fromCharCode(
        "A".charCodeAt(0) + randomAlternativeIndex
      );
      cy.get(`label[for="${randomAlternative}"]`).click();
      cy.wait(3000);
      cy.get('button:contains("Próxima")').click();
    });
  });
});
