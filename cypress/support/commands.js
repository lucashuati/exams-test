import {
  API_HOST,
  COLLECTION_ID,
  INTEGRATION_USERNAME,
  INTEGRATION_PASSWORD,
} from "./consts";

let LOCAL_STORAGE_MEMORY = {};

Cypress.Commands.add("saveLocalStorage", () => {
  Object.keys(localStorage).forEach((key) => {
    LOCAL_STORAGE_MEMORY[key] = localStorage[key];
  });
});

Cypress.Commands.add("restoreLocalStorage", () => {
  Object.keys(LOCAL_STORAGE_MEMORY).forEach((key) => {
    localStorage.setItem(key, LOCAL_STORAGE_MEMORY[key]);
  });
});

function getRandonUserName() {
  var text = "";
  var possible =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < 16; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}

Cypress.Commands.add("getToken", () => {
  //Criação do token
  cy.request("POST", `${API_HOST}/v1/login`, {
    username: INTEGRATION_USERNAME,
    password: INTEGRATION_PASSWORD,
    set_new_client: true,
    client_id: "9e76360f-8ad1-4b0b-8d9f-57408d406142",
  }).then((response) => {
    cy.writeFile("cypress/fixtures/authUser.json", response.body);
  });

  // cy.fixture("authUser").should((user) => {
  //   expect(user.token).to.exist;
  // });
});

Cypress.Commands.add("createUser", () => {
  //Criação de usuário de forma automatizada
  cy.fixture("authUser").should((user) => {
    //Criar novo usuário
    var userName = getRandonUserName();
    var newUserRequest = {
      user: {
        name: userName,
        email: userName + "@educat.net.br",
        public_identifier: userName,
        extra: "{}",
      },
      collection: COLLECTION_ID,
    };

    //Chamar API para crição do usuário
    var customOptions = {
      method: "POST",
      url: `${API_HOST}/v1/applications`,
      body: newUserRequest,
      headers: {
        "Content-Type": "application/json",
        Authorization: "JWT " + user.token,
      },
    };
    cy.request(customOptions).then((response) => {
      cy.writeFile("cypress/fixtures/user.json", response.body);
      console.info("Created user: " + response.body);
    });

    cy.wait(500);

    cy.fixture("user").should((newUser) => {
      var resetPassword = {
        user: newUser.user.id,
        new_password: userName,
      };

      customOptions = {
        method: "POST",
        url: `${API_HOST}/v1/reset_password`,
        body: resetPassword,
        headers: {
          authorization: "JWT " + user.token,
        },
      };

      cy.request(customOptions).then((response) => {
        cy.writeFile("cypress/fixtures/pwd.json", response.body);
      });
    });

    cy.wait(500);

    // cy.fixture("user").should((newUser) => {
    //   console.log(newUser.user.id);
    //   expect(newUser.user.id).to.exist;
    // });
  });
});
